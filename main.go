package main

import (
	"image"
	"os"

	_ "image/png"
	"github.com/faiface/pixel"
	"github.com/faiface/pixel/pixelgl"
)

func loadPicture(path string) (*pixel.PictureData, error) {
	file, err := os.Open(path)

	if err != nil {
		return nil, err
	}

	defer file.Close()

	img, _, err := image.Decode(file)

	if err != nil {
		return nil, err
	}

	return pixel.PictureDataFromImage(img), nil
}

func run() {
	cfg := pixelgl.WindowConfig{
		Title:  "Game",
		Bounds: pixel.R(0, 0, 1200, 800),
		VSync:  true,
	}

	win, err := pixelgl.NewWindow(cfg)

	if err != nil {
		panic(err)
	}

	spritesheet, err := loadPicture("sprite-sheet.png")
	r := pixel.R(25, 25, 50, 50)
	sprite := pixel.NewSprite(spritesheet, r)

	for !win.Closed() {
		v := pixel.V(100, 100)

		sprite.Draw(win, pixel.IM.Moved(v))
		win.Update()
	}
}

func main() {
	pixelgl.Run(run)
}
